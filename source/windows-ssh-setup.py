import random
import textwrap
import os

username = input("User: ")
if input(f"Confirm {username} (y/n)? : ") != 'y':
    exit(0)

config = ""

randset = set()
i = 0
while i < 3:
    num = random.randint(1, 30)
    if num in randset:
        continue
    i += 1

    config += textwrap.dedent(
        f"""
        Host rc{num:02d}xcs213.managed.mst.edu
            Hostname rc{num:02d}xcs213.managed.mst.edu
            User {username}    
        """
    )

if not os.path.exists(f"c:\\users\\{username}\\.ssh"):
    os.mkdir(f"c:\\users\\{username}\\.ssh")

with open(f"c:\\users\\{username}\\.ssh\\config", "a") as fp:
    fp.write(config)


    