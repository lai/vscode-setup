.. VSCode Setup documentation master file, created by
   sphinx-quickstart on Tue Aug 24 12:33:05 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

VSCode Setup
========================================
If you are on campus CLC, install Python3 and VSCode through Apps Anywhere.
If you are using personal computer, install VSCode through https://code.visualstudio.com/ and python3 through Windows App Store
Download :download:`windows-ssh-setup.py` and save the file. Then run the file with python

1. Enable Auto-save

.. image:: https://i.imgur.com/Y4031WX.gif

2. Install the Remote.SSH extension

.. image:: https://i.imgur.com/JDNfCwL.gif

3. Lock file in /tmp

.. image:: https://i.imgur.com/fR8efUR.gif

4. Login

.. image:: https://i.imgur.com/A4iCrJZ.gif

.. toctree::
   :maxdepth: 2
   :caption: Contents:

   ssh-setup.rst


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
