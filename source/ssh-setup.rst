Setup VSCode/SSH
=================

If you are on campus CLC, install Python3 and VSCode through Apps Anywhere.
Download :download:`windows-ssh-setup.py` and save the file. Then run the file with python

1. Enable Auto-save

.. image:: https://i.imgur.com/Y4031WX.gif

2. Install the Remote.SSH extension

.. image:: https://i.imgur.com/JDNfCwL.gif

3. Lock file in /tmp

.. image:: https://i.imgur.com/fR8efUR.gif

4. Login

.. image:: https://i.imgur.com/A4iCrJZ.gif
